import unittest
from pyspark.sql import SparkSession
from py4j.java_gateway import JavaGateway

class ConfigSession(unittest.TestCase):

    print("hello")
    @classmethod
    def setUpClass(cls):
        # Créer une session Spark pour tous les tests
        cls.spark = SparkSession.builder \
            .appName("Test Spark App") \
            .master("local[2]") \
            .getOrCreate()
   
class TestSparkApp(ConfigSession):
    def test_dataframe_creation(self):
        # Créer une DataFrame et vérifier le contenu
        data = [("Hello, World!",)]
        df = self.spark.createDataFrame(data, ["Golden Line Spark"])
        self.assertEqual(df.count(), 1)
        self.assertEqual(df.first()["Golden Line Spark"], "Hello, World!")

    def test_rdd_operations(self):
        # Test des opérations RDD
        data = [1, 2, 3, 4, 8]
        rdd = self.spark.sparkContext.parallelize(data)
        self.assertEqual(rdd.sum(), sum(data))
        self.assertEqual(rdd.count(), len(data))

    @classmethod
    def tearDownClass(cls):
        # Arrêter la session Spark après tous les tests
        cls.spark.stop()

if __name__ == '__main__':
    unittest.main()
