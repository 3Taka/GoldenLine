
## Prerequis et actions - Docker / Gitlab 
- **Docker** : 
1. S'incrire sur docker hub 
2. Generer un token 
3. Créer un repository 

--

- **GitLab** : 
1. Avoir un compte Gitlab.
2. Parametrer la clé SSH entre Gitlab et la machine locale.
2. Avoir cloné le code sur un repository en local 

--

- **CI/CD** :
1. Parametrer les variables d'environnement de la ci/cd projet : 
AWS : 
$AWS_ACCESS_KEY_ID (masked mode)
$AWS_SECRET_ACCESS_ID (masked mode)
$AWS_REPOSITORY_URL (repo ecr) 
DOCKER :
$DOCKER_REGISTRY_PASS (masked mode)
$DOCKER_REGISTRY_USER
$DOCKER_REGISTRY

2. Modifier la version de l'image à build dans le fichier de la ci/cd

3. Add, commit et push la modification. 


