from pyspark.sql import SparkSession
from pyspark import SparkContext, SparkConf
from pymongo import MongoClient
import boto3

__all__ = ["SparkSession"]

# Création d'une instance de SparkSession
spark = SparkSession.builder \
    .appName("HelloWorld") \
    .getOrCreate()

# Création d'une DataFrame 
data = [("Hello, World!",)]
df = spark.createDataFrame(data, ["GoldenLine Spark"])
df.show()

# Création d'un RDD (Resilient Distributed Dataset) à partir d'une liste
data = [1, 2, 3, 4, 8]
rdd = spark.sparkContext.parallelize(data)

# Calcul de la somme des éléments
somme = rdd.sum()

# Calcul le nombre d'élement
decompte = rdd.count()

print("Liste d'éléments :", data)
print("Somme =", somme)
print("Nombre d'élements =", decompte)

# Arrêt de la session Spark
spark.stop()

print()

#parametrage des variables d'environnement pour aws cli
print("Parametrage des identifiants du provider...")
aws_access_key_id = input("Entrez votre Access Key d'utilisateur :")
aws_secret_access_key = input("Entrez votre Secret Access Key d'utilisateur :")
aws_session_token = input("Entrez votre AWS Session Token (laissez vide si vous n'en avez pas) : ")
aws_region = input("Entrez votre région d'utilisateur :")

#parametrage de la session
session = boto3.session(
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key,
    aws_session_token=aws_session_token,
    aws_region=aws_region
)

print("Vous êtes connecté à aws")

#CONNEXION DOCDB -  Mongo

#parametrage du client
db_client = boto3.client('docdb', region_name=aws_region)

# Récupérez l'uri de l'instance DocumentDB
response = db_client.describe_db_instances()
endpoint = response['DBInstances'][0]['Endpoint']['Address']['port']


uri_mongodb = f"mongodb://{endpoint}/"

print("L'uri de la base mongo sur docdb est :", uri_mongodb)

#connection à la base de donnée
client = MongoClient(uri_mongodb)

print("Vous êtes connecté à votre docdb mongo")

print()

#CONNEXION EMR - Spark

emr_client = boto3.client('emr', region_name='votre_region_aws')

#recuperation de l'id du cluster via son nom - parametré dans le code HCL
response = emr_client.list_clusters
for cluster in response['Clusters']:
    if cluster['Name'] == 'mon_cluster_spark':
        # Récupérez l'ID du cluster
        cluster_id = cluster['Id']
    break

#Recuperation de l'uri
response = emr_client.describe_cluster(ClusterId=cluster_id)
master_public_dns = response['Cluster']['MasterPublicDnsName']

uri_spark = master_public_dns

#SparkContext
conf = SparkConf().setAppName("Goldenline").setMaster(f"spark://{master_public_dns}:7077")
sc = SparkContext(conf=conf)

print("Vous êtes connecté à votre Cluster Spark")