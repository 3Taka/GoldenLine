
#ECS - App GoldenLine
#Cluster 

#Instance


resource "aws_launch_configuration" "ecs" {
name_prefix = "ecs-intances"
iam_instance_profile = var.ecs_instance_profile
image_id = "ami-0d5eff06f840b45e9"
security_groups = [aws_security_group.app_security_group.id]

instance_type = var.app_instance_type
key_name = data.aws_key_pair.key_ec2.key_name
user_data     = <<-EOF
  #!/bin/bash
  echo ECS_CLUSTER=${aws_ecs_cluster.my_ecs_cluster.name} >> /etc/ecs/ecs.config
  EOF
}

#Autoscalling_group
resource "aws_autoscaling_group" "ecs_asg" {
 name = "ecs_asg"

 launch_configuration = aws_launch_configuration.ecs.id
 vpc_zone_identifier = [aws_subnet.my_subnet_app.id]
 desired_capacity    = 1
 max_size            = 3
 min_size            = 1
}


#TASK container
locals {
  ecs_target_port = 22
  ecs_container_name = "goldenlin"
  ecs_cpu = 512
  ecs_memory = 1024
  ecs_c1_cpu = 1
  ecs_c1_memory = 1
}

#data "template_file" "task_def_generated" {
#  template = file("./task_definition.tpl")
#    vars = {
#    port                = local.ecs_target_port
#    name                = local.ecs_container_name
#    image               = local.ecs_image
#    cpu                 = local.ecs_cpu
#    memory              = local.ecs_memory
#    desired_count       = local.ecs_desired_count 
#}

#resource "local_file" "output_task_def" {
#  content         = data.template_file.task_def_generated.rendered
#  file_permission = "777"
#  filename        = "./task-definitions.tpl"
#}

#Cluster 
resource "aws_ecs_cluster" "my_ecs_cluster" {
  name = var.ecs_cluster_name 
}
#TaskPolicy
resource "aws_iam_role_policy" "ecs_task_policy" {
  name   = "ecs_task_policy"
  role   = var.ecs_role
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect   = "Allow",
        Action   = [
                "ecs:RegisterTaskDefinition",
                "ecs:DescribeTaskDefinition",
                "ecs:DeregisterTaskDefinition",
                "ecs:UpdateService"
        ],
        Resource = "*",
      },
      
    ],
  })
}

#TASK
resource "aws_ecs_task_definition" "my_task_definition" {
  family                   = "my-task-family"
  execution_role_arn       = var.ecs_role_arn
  network_mode             = "awsvpc" 
  requires_compatibilities = ["FARGATE"]
  cpu                      = local.ecs_cpu
  memory                   = local.ecs_memory

  container_definitions    = jsonencode([
    {
      name 	= local.ecs_container_name
      image 	= "737935841387.dkr.ecr.us-east-1.amazonaws.com/ecr:latest"
      cpu 	= local.ecs_c1_cpu
      memory 	= local.ecs_c1_memory
      essential : true,
      environment = [
      #Intégration des endpoints pour interconnexion avec bdd et cluster spark
        {
          name = "mongodb_endpoint"
          value = "${aws_docdb_cluster.mongodb_cluster.endpoint}:27017"
        },           
        {
          name = "sparkmaster_endpoint"
          value = "${aws_emr_cluster.spark_cluster.master_public_dns}:18080"
        }
      ]
      portMappings = [
        {
          containerPort = local.ecs_target_port
          hostPort = local.ecs_target_port
          protocol = "tcp"
        }
      ]
    }
  ])
}

# Service
resource "aws_ecs_service" "my_ecs_service" {
  name            = "my_ecs_service" 
  cluster         = aws_ecs_cluster.my_ecs_cluster.id
  task_definition = aws_ecs_task_definition.my_task_definition.arn
  desired_count   = 1 
  # Reseau
  network_configuration {
    subnets         = [aws_subnet.my_subnet_app.id]
    security_groups = [aws_security_group.app_security_group.id] 
  }
}
