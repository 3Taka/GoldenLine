#SPARK Master - 
resource "aws_security_group" "spark_m_security_group" {
  name        = "sg_master"
  description = "spark_security_group_master"
  vpc_id      = data.aws_vpc.my_vpc.id
}
#Inbound rules
resource "aws_security_group_rule" "allow_spark_m_access" {
  type        = "ingress"
  from_port   = 18080
  to_port     = 18080
  protocol    = "tcp"
  cidr_blocks = [var.cidr_subnet_app]
  security_group_id = aws_security_group.spark_m_security_group.id
}
#Outbound rules 
resource "aws_security_group_rule" "allow_spark_m_out" {
    type        = "egress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.spark_m_security_group.id
}

#SPARK Core - 
resource "aws_security_group" "spark_c_security_group" {
  name        = "sg_core"
  description = "spark_security_group_core"
  vpc_id      = data.aws_vpc.my_vpc.id

  depends_on = [aws_security_group.spark_m_security_group]
}
#Inbound rules
resource "aws_security_group_rule" "allow_spark_c_access" {
  type        = "ingress"
  from_port   = 7077
  to_port     = 7077
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.spark_c_security_group.id
}
#Outbound rules 
resource "aws_security_group_rule" "allow_spark_c_out" {
    type        = "egress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.spark_c_security_group.id
}


#MongoDB - 
resource "aws_security_group" "mongodb_security_group" {
  name        = "sg_mongo"
  description = "mongo_security_group"
  vpc_id      = data.aws_vpc.my_vpc.id
}
#Inbound rules
resource "aws_security_group_rule" "allow_mongodb_access" {
    type        = "ingress"
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = [var.cidr_subnet_app]
    security_group_id = aws_security_group.mongodb_security_group.id
}
#Outbound rules 
resource "aws_security_group_rule" "allow_mongodb_out" {
    type        = "egress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.mongodb_security_group.id
}


#GoldenLine - 
resource "aws_security_group" "app_security_group" {
  name        = "sg_app"
  description = "app_security_group"
  vpc_id      = data.aws_vpc.my_vpc.id
}
#Inbound rules
resource "aws_security_group_rule" "allow_app_access_ssh" {
    type        = "ingress"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.app_security_group.id
}
#Outbound rules 
resource "aws_security_group_rule" "allow_app_out" {
    type        = "egress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.app_security_group.id
}           