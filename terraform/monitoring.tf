#Création du bucket - logs EMR
#resource "aws_s3_bucket" "my_bucket" {
#  bucket = "s3GoldenLine"
#  acl    = "private"

#  versioning {
#    enabled = true
#  }

#  lifecycle_rule {
#    id      = "delete-older-than-30-days"
#    enabled = true
#  }

#  tags = {
#    Name        = "My S3 Bucket"
#    Environment = "Production"
#  }
#}

locals {
  role_arn_logs_db_configuration = "arn:aws:iam::737935841387:role/DB_S3"
}

resource "aws_s3_object" "directory_emr_logs" {
  bucket = var.my_bucket
  key    = "emr_logs/"
}

resource "aws_s3_object" "directory_docdb_logs" {
  bucket = var.my_bucket
  key    = "docdb_logs/"
}

#EMR CLuster SPARK - 

#ressources pour logs
resource "aws_cloudwatch_log_group" "emr_log_group" {
  name              = "emr_log_group"
  retention_in_days = var.retention_days_emr
}

resource "aws_cloudwatch_log_stream" "emr_log_stream" {
  name           = "emr_log_stream"
  log_group_name = aws_cloudwatch_log_group.emr_log_group.name
}

#Alarmes
resource "aws_sns_topic" "emr_alarm_topic" {
  name = "my_emr_topic"
}

resource "aws_sns_topic_subscription" "emr_alarm_subscription" {
  topic_arn = aws_sns_topic.emr_alarm_topic.arn
  protocol  = "email"
  endpoint  = var.mail_endpoint
}

resource "aws_cloudwatch_metric_alarm" "emr_cpu_utilization_high" {
  alarm_name          = "emr-cpu-utilization-high"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ElasticMapReduce"
  period              = 300
  statistic           = "Average"
  threshold           = var.emr_cpu_threshold
  alarm_description   = "Alarm CPU utilisation high"
  alarm_actions       = [aws_sns_topic.emr_alarm_topic.arn]
  dimensions = {
    JobFlowId = aws_emr_cluster.spark_cluster.id
  }
}

resource "aws_cloudwatch_metric_alarm" "emr_memory_utilization_low" {
  alarm_name          = "emr-memory-utilization-low"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "FreeableMemory"
  namespace           = "AWS/DocDB"
  period              = 300
  statistic           = "Average"
  threshold           = var.emr_memory_threshold
  alarm_description   = "Alarm when freeable memory is too low"
  alarm_actions       = [aws_sns_topic.emr_alarm_topic.arn]
  dimensions = {
    JobFlowId = aws_emr_cluster.spark_cluster.id
  }
}

resource "aws_cloudwatch_metric_alarm" "emr_storage_utilization_high" {
  alarm_name          = "emr-storage-utilization-high"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "StorageUtilization" 
  namespace           = "AWS/ElasticMapReduce" 
  period              = 300
  statistic           = "Average"
  threshold           = var.emr_storage_threshold
  alarm_description   = "Alarm when storage utilisation high"
  alarm_actions       = [aws_sns_topic.emr_alarm_topic.arn]
  dimensions = {
    JobFlowId = aws_emr_cluster.spark_cluster.id
  }
}


#MONGO DB - 

#Ressources pour logs
resource "aws_cloudwatch_log_group" "docdb_log_group" {
  name              = "/aws/docdb/${aws_docdb_cluster.mongodb_cluster.id}"
  retention_in_days = 7
}

resource "aws_cloudwatch_log_stream" "docdb_log_stream" {
  name           = "docdb_log_stream"
  log_group_name = aws_cloudwatch_log_group.docdb_log_group.name
}

#resource "aws_docdb_cluster_parameter_group" "my_docdb_cluster_parameter_group" {
#  family = "docdb5.0"

#  parameter {
#    name  = "audit_log_roleArn"
#    value = local.role_arn_logs_db_configuration
#  }

#   parameter {
#    name  = "s3Logging.enabled"
#    value = "true"
#  }

#  parameter {
#    name  = "s3Logging.bucketName"
#    value = data.aws_s3_bucket.my_bucket
#  }

#  parameter {
#    name  = "s3Logging.bucketPrefix"
#    value = "docdb-logs/"
#  }
#}

#Alarmes 
resource "aws_sns_topic" "docdb_alarm_topic" {
  name = "docdb-alarm-topic"
}

resource "aws_sns_topic_subscription" "docdb_alarm_subscription" {
  topic_arn = aws_sns_topic.docdb_alarm_topic.arn
  protocol  = "email"
  endpoint  = var.mail_endpoint
}

resource "aws_cloudwatch_metric_alarm" "docdb_cpu_utilization_high" {
  alarm_name          = "docdb-cpu-utilization-high"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/DocDB"
  period              = 300
  statistic           = "Average"
  threshold           = var.docdb_cpu_threshold
  alarm_description   = "Alarm when CPU exceeds 85%"
  alarm_actions       = [aws_sns_topic.docdb_alarm_topic.arn]
  dimensions = {
    DBClusterIdentifier = aws_docdb_cluster.mongodb_cluster.cluster_identifier
  }
}

resource "aws_cloudwatch_metric_alarm" "docdb_memory_utilization_low" {
  alarm_name          = "docdb-memory-utilization-low"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "FreeableMemory"
  namespace           = "AWS/DocDB"
  period              = 300
  statistic           = "Average"
  threshold           = var.docdb_memory_threshold
  alarm_description   = "Alarm when freeable memory is too low"
  alarm_actions       = [aws_sns_topic.docdb_alarm_topic.arn]
  dimensions = {
    DBClusterIdentifier = aws_docdb_cluster.mongodb_cluster.cluster_identifier
  }
}

resource "aws_cloudwatch_metric_alarm" "docdb_storage_utilization_low" {
  alarm_name          = "docdb-storage-utilization-low"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/DocDB"
  period              = 300
  statistic           = "Average"
  threshold           = var.docdb_storage_threshold
  alarm_description   = "Alarm when free storage space is too low"
  alarm_actions       = [aws_sns_topic.docdb_alarm_topic.arn]
  dimensions = {
    DBClusterIdentifier = aws_docdb_cluster.mongodb_cluster.cluster_identifier
  }
}
