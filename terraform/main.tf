provider "aws" {
  region                      = var.region
}

#CLE SSH
#resource "aws_key_pair" "key_ec2" {
#  key_name   = data.key_ec2.key_name
#  public_key = file(var.public_key_path)
#}

# PARAMETRAGE RESEAU VPC -

# 1. Création du VPC
resource "aws_vpc" "my_vpc" {
  cidr_block = var.cidr_vpc
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "MyVPC1"
  }
}

# 2. Création routeur et gateway associéé
resource "aws_internet_gateway" "my_igw" {
  vpc_id = data.aws_vpc.my_vpc.id
  tags = {
    Name = "MyGW1"
  }
}

resource "aws_route_table" "my_route" {
  vpc_id = data.aws_vpc.my_vpc.id 
  route {
    cidr_block = var.cidr_route
    gateway_id = aws_internet_gateway.my_igw.id 
  }
  tags = {
    Name = "MyRouteur1"
  }
}

resource "aws_main_route_table_association" "main_association" {
  vpc_id     = data.aws_vpc.my_vpc.id
  route_table_id = aws_route_table.my_route.id
}

# 3. Création des sous-réseaux - 
resource "aws_subnet" "my_subnet_spark" {
  vpc_id     = data.aws_vpc.my_vpc.id
  cidr_block = var.cidr_subnet_spark
  availability_zone = var.availability_zone
  tags = {
    Name = "MySubnet_Spark1"
  }
}
resource "aws_subnet" "my_subnet_mongodb1" {
  vpc_id     = data.aws_vpc.my_vpc.id
  cidr_block = var.cidr_subnet_mongodb1
  availability_zone = var.availability_zone
  tags = {
    Name = "MySubnet_mongodb1"
  }
}
resource "aws_subnet" "my_subnet_mongodb2" {
  vpc_id     = data.aws_vpc.my_vpc.id
  cidr_block = var.cidr_subnet_mongodb2
  availability_zone = var.availability_zone2
  tags = {
    Name = "MySubnet_mongodb2"
  }
}
resource "aws_subnet" "my_subnet_app" {
  vpc_id     = data.aws_vpc.my_vpc.id
  cidr_block = var.cidr_subnet_app
  availability_zone = var.availability_zone
  tags = {
    Name = "MySubnet_app"
  }
}

# 4. Association de la table de routage aux sous-réseaux
resource "aws_route_table_association" "route_my_subnet_spark" {
  subnet_id      = aws_subnet.my_subnet_spark.id
  route_table_id = aws_route_table.my_route.id
}
resource "aws_route_table_association" "my_route_my_subnet_mongodb1" {
  subnet_id      = aws_subnet.my_subnet_mongodb1.id
  route_table_id = aws_route_table.my_route.id
}
resource "aws_route_table_association" "my_route_my_subnet_mongodb2" {
  subnet_id      = aws_subnet.my_subnet_mongodb2.id
  route_table_id = aws_route_table.my_route.id
}
resource "aws_route_table_association" "my_route_my_subnet_app" {
  subnet_id      = aws_subnet.my_subnet_app.id
  route_table_id = aws_route_table.my_route.id
}


# PARAMETRAGE CLUSTERS -

#Spark Cluster
resource "aws_emr_cluster" "spark_cluster" {
  name                           = "mon_cluster_spark"
  release_label                  = var.version_emr 
  applications                   = ["Hadoop","Spark"]
  service_role                   = var.emr_service_role
  log_uri                        = "s3://${var.my_bucket}/emr_logs"

  ec2_attributes {
    subnet_id                         = aws_subnet.my_subnet_spark.id
    emr_managed_master_security_group = aws_security_group.spark_m_security_group.id
    emr_managed_slave_security_group  = aws_security_group.spark_m_security_group.id
    key_name                          = data.aws_key_pair.key_ec2.key_name
    instance_profile                  = var.ec2_instance_profile
  }

  master_instance_group {
    instance_type = var.master_instance_type
  }

  core_instance_group {
    instance_type  = var.core_instance_type
    instance_count = var.sparkcore_instance_count

    ebs_config {
        size = "40"
        type = "gp2"
        volumes_per_instance = 1
    }
  }
}

# MongoDB - Cluster
#Subnet groupe
resource "aws_docdb_subnet_group" "mongodb_subnet_group" {
  subnet_ids = [ 
    aws_subnet.my_subnet_mongodb1.id,
    aws_subnet.my_subnet_mongodb2.id,
  ]
}
#Cluster
resource "aws_docdb_cluster" "mongodb_cluster" {
  cluster_identifier            = "mongodb-cluster"
  engine                        = "docdb"
  #db_cluster_parameter_group_name = aws_docdb_cluster_parameter_group.my_docdb_cluster_parameter_group.name
  master_username               = var.master_username
  master_password               = var.master_password 
  backup_retention_period       = var.backup_retention_period
  preferred_backup_window       = var.backup_window
  skip_final_snapshot           = true
  db_subnet_group_name          = aws_docdb_subnet_group.mongodb_subnet_group.name
  vpc_security_group_ids        = [aws_security_group.mongodb_security_group.id]
  storage_encrypted             = true
  enabled_cloudwatch_logs_exports = ["audit"]
}
#Instances
resource "aws_docdb_cluster_instance" "mongodb_instance" {
  count                  = var.mongo_instance_count
  identifier             = "mongodb-instance-${count.index}"
  instance_class         = var.instance_class
  cluster_identifier     = aws_docdb_cluster.mongodb_cluster.id
  availability_zone      = var.availability_zone
}

