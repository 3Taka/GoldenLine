
## Prerequis et actions - AWS / Terraform 

- **AWS** : 
1. S'incrire en tant que **root**.
2. Créer une **clé SSH** reliée à la machine locale.
2. Créer le (ou les) **groupe**(s) d'utilisateur.
3. Créer les **rôles** et leurs **polices**, correspondants aux ressources à construire et gérer (EC2 - EMR - ECS* - S3) et le raccorder au(x) groupe(s) d'utilisateur(s). 
4. Créer le ou les **utilisateur**(s) et le raccorder aux groupe(s).
5. Créer le repository **ECR** (nom : ecr)
6. Créer le **S3** (nom : s3goldenline) 
7. Telecharger **AWS CLI** en local et le configurer avec les identifiants de l'utilisateur AWS. 


**Précisions dans la doc technique*

--


- **Terraform** : 
1. Telecharger **terraform** en local
2. Avoir telecharger le code et être positionné dans le dossier ./terraform
2. Terraform **init** (initialisation du back en fonction des ressources et du code employé)
3. Terraform **plan** (verification du code et du plan de déploiement)
4. Terraform **apply** (application du plan)
5. Terraform **destroy** (destruction des ressources pour plan net à la reconstruction de l'infrastructure)
