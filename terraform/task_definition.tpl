container_definitions    = jsonencode([
    {
      name 	= local.ecs_container_name
      image 	= "737935841387.dkr.ecr.us-east-1.amazonaws.com/ecr:latest"
      cpu 	= local.ecs_c1_cpu
      memory 	= local.ecs_c1_memory
      essential : true,
      environment = [
      #Intégration des endpoints pour interconnexion avec bdd et cluster spark
        {
          name = "mongodb_endpoint"
          value = "${aws_docdb_cluster.mongodb_cluster.endpoint}:27017"
        },           
        {
          name = "sparkmaster_endpoint"
          value = "${aws_emr_cluster.spark_cluster.master_public_dns}:18080"
        }
      ]
      portMappings = [
        {
          containerPort = local.ecs_target_port
          hostPort = local.ecs_target_port
          protocol = "tcp"
        }
      ]
    }
])
