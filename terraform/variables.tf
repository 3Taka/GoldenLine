#PROVIDER
variable "region" {
  default = "us-east-1"
}


#VPC
variable "cidr_vpc" {
  default = "10.0.0.0/24"
}
variable "cidr_route" {
  default = "0.0.0.0/0"
}

#S3
variable "my_bucket" {
  default = "s3goldenline"
}

#SUBNET
variable "availability_zone" {
  default = "us-east-1a"
}

variable "availability_zone2" {
  default = "us-east-1b"
}

variable "cidr_subnet_spark" {
  default = "10.0.0.0/26"
  sensitive = true
}

variable "cidr_subnet_mongodb1" {
  default = "10.0.0.64/26"
  sensitive = true
}

variable "cidr_subnet_mongodb2" {
  default = "10.0.0.128/26"
  sensitive = true
}

variable "cidr_subnet_app" {
  default = "10.0.0.192/26"
  sensitive = true
}


#SECURITY GROUPS 
variable "cidr_sg_spark" {
  default = "0.0.0.0/0"
  sensitive = true
}
variable "cidr_sg_mongo" {
  default = "0.0.0.0/0"
  sensitive = true
}
variable "cidr_sg_app" {
  default = "0.0.0.0/0"
  sensitive = true
}


#SSH KEY
variable "key_name" {
  default = "key_ec2"
  sensitive = true
}
variable "public_key_path" {
  default = "~/.ssh/id_rsa.pub"
  sensitive = true
}


#CLUSTER SPARK - 
variable "version_emr" {
  default = "emr-6.4.0"
}
variable "emr_service_role" {
  default = "EMR_EC2_Role"
}

#Instances 
variable "ec2_instance_profile" {
  default = "EC2_4EMR"
}

#master 
variable "master_instance_type" {
  default = "m4.large"
}

#core 
variable "core_instance_type" {
  default = "c4.large"
}
variable "sparkcore_instance_count" {
  default = 1
}

#Monitoring 
#Logs 
variable "retention_days_emr" {
  default = 30
}

#Alarmes 
variable "mail_endpoint" {
  default = "alarm_goldenline@gmail.com"
}

#CPU
variable "emr_cpu_threshold" {
  default = 85
}

#Memory
variable "emr_memory_threshold" {
  default = 15 * 1024 * 1024 * 1024
}

#Storage
variable "emr_storage_threshold" {
  default = 85
}


#MONGO - Cluster DB - 
variable "mongo_instance_count" {
  default = 2
}
variable "instance_class" {
  default = "db.r5.large"
}

#login
variable "master_username" {
  default = "adminuser"
  sensitive = true
}
variable "master_password" {
  default = "adminpassw4rd!"
  sensitive = true
}

#Rules 
variable "backup_retention_period" {
  default = 5
}
variable "backup_window" {
  default = "05:00-07:00"
}

#Monitoring 
#Logs 
variable "retention_days_docdb" {
  default = 7
}

#Alarmes 

#CPU
variable "docdb_cpu_threshold" {
  default = 85
}

#Memory
variable "docdb_memory_threshold" {
  default = 15 * 1024 * 1024 * 1024
}

#Storage
variable "docdb_storage_threshold" {
  default = 85
}


#ECS - 
variable "ecs_cluster_name" {
  default = "my_cluster_ecs"
}

#Instances 
variable "ecs_instance_profile" {
  default = "EC2_4EMR"
}

variable "app_instance_type" {
  default = "t2.micro"
}


#Task

variable "ecs_role" {
  default = "ECS_Role"
}

variable "ecs_role_arn" {
  default = "arn:aws:iam::737935841387:role/ECS_Role"
}

variable "container_config_path" {
  type    = string
  default = "./container_definition.json"
}

#Bucket 
variable "expiration_" {
  default = "t2.micro"
}
